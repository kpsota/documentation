## CleverAnalytics AreaMapper ##

AreaMapper is tool which can help you to prepare your bussines data for visualization in CleverAnalytics platform.
AreaMapper adds ID's of polygons (e.g., Administrative units) to your data (e.g., Transactions). It helps to aggregate your point data to polygon (area) representation.

You can map points to your own area data or you can use already prepared data dimension from us (e.g. administrative units).


### Running AreaMapper ###

There are different ways how to execute AreaMapper:

As a standard python script in command line:
```
python3 areamapper.py --config my_config.json
```

Build docker image using Docker_local file
```
docker build -t areamapper -f Dockerfile_local .
docker run -v /home/karel/can_dev/git/can-areamapper/testdata:/data areamapper --config /data/config.json
```

AreaMapper is also prepared to be deployed as a component into the Keboola Connection


### Configuration ###

AreaMapper needs a configuration to run - specifically you need to pass a path of the JSON file in to the `--config` parameter. You can find some config files in examples folder.

#### Configuration specification ####

`input_points`

`input_areas`

`output`


`input_points.filetype`

`input_points.filename`

`input_points.delimiter`

`input_points.geom_field_x`

`input_points.geom_field_y`

`input_points.join_fieldname`


`input_areas.filetype`

`input_areas.filename`

`input_areas.delimiter`

`input_areas.geom_fieldname`

`input_areas.primary_key_fieldname`

`input_areas.join_fieldname`


`output.filename`

`output.delimiter`

`output.not_matched_value`